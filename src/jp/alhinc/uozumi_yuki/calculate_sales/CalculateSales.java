package jp.alhinc.uozumi_yuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
;

	public static void main(String[] args){
		System.out.println("ここにあるファイルを開きます =>" + args[0]);



	    BufferedReader br = null;
	    BufferedWriter bw = null;
	    Map<String, Long> map = new LinkedHashMap<String, Long>();
	    Map<String, String> sales = new LinkedHashMap<String, String>();
	    LineNumberReader nr = null;

	    try{
		    File file = new File(args[0],"branch.lst");
		    
		    if(!(file.exists())){
		    	System.out.println("支店定義ファイルが存在しません");
		    	return;
	    	}
		    
		    
		    
	        FileReader  fr = new FileReader(file);
		    br = new BufferedReader(fr);

		    //エラー処理ファイル

		    

		    String line;

		    while((line = br.readLine()) != null){
		    	String[] items = line.split(",");

		    	//エラー処理・フォーマット

		    	if(items.length != 2){
		    		System.out.println("支店定義ファイルのフォーマットが不正です");
		    		return;
		    	}

		    	if(!(items[0].matches("^[0-9]{3}$"))){
		    		System.out.println("支店定義ファイルのフォーマットが不正です");
		    		return;
		    	}

				 long l = 0;

			     map.put(items[0], l);
			     sales.put(items[0], items[1]);
		    }

		    System.out.println(map.keySet());

			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
			}finally{
				if(br != null)
					try{
						br.close();
					}catch(IOException e){
						System.out.println("closeできませんでした");
				}
			}

	    File targetFolder = new File(args[0]);
	    List<Integer> number = new ArrayList<Integer>();


	    for(String name : targetFolder.list()){

	    	if(name.matches("^\\d{8}.rcd$")){
	    		System.out.println(name);

	    		number.add(Integer.parseInt(name.substring(0,8)));

	    		Collections.sort(number);

	    		for (int i = 1; i < number.size(); i++){
	    			if ( number.get(i) != number.get(i-1) + 1){
	    				System.out.println("売上ファイル名が連番になっていません");
	    				return;
	    			}
	    		}
	    		//エラー処理　連番

	    		//エラー処理　3行

	    		try{
	    			File file = new File(args[0],name);
				    FileReader  fr = new FileReader(file);
	    			nr = new LineNumberReader(fr);

	    			String line;

	    			while((line = nr.readLine()) != null){

					   if(!(nr.getLineNumber() <= 3)){
						   System.out.println("<該当ファイル名>のフォーマットが不正です");
						   return;
					   }
					   System.out.println(line);
	    			}


	    		}catch(IOException e){
	    			System.out.println("予期せぬエラーが発生しました");
	    		}finally{
	    			if(nr != null)
	    				try{
	    					br.close();
	    				}catch(IOException e){
	    					System.out.println("closeできませんでした");
	    				}
	    		}

	    		try{
			       File file = new File(args[0],name);
			       FileReader  fr = new FileReader(file);
				   br = new BufferedReader(fr);

				   String code = br.readLine();
				   String area = br.readLine();
				   String sv = String.valueOf(code);
				   int num = Integer.parseInt(area);

				   //エラー処理支店コード

				   if(!(sv.matches("^(0[0-5]*)$"))){
					   System.out.println("<該当ファイル名>の支店コードが不正です");
					   return;
				   }

				   System.out.println("------------------------------");
				   System.out.println(code);
				   System.out.println(area);

				   map.get(code);
				   long in = map.get(code);
				   long total = in + num;

 				   map.put(code, total);

				   System.out.println(map.get(code));

				   //合計金額取得

				   //エラー処理10桁

				   String str = Long.toString(total);

				   long limit = 9999999999L;
				   String digit = String.valueOf(limit);

				   if(!(str.length() <= digit.length())){
					   System.out.println("合計金額が10桁を超えました");
					   return;
				   }

	    		}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
				}finally{
					if(br != null)
						try{
							br.close();
						}catch(IOException e){
							System.out.println("closeできませんでした");
					}
				}
	    	}
	    }

	    for (String key : map.keySet()){
	        System.out.println(key + ":" + map.get(key));
	    }

	    System.out.println(map);
	    System.out.println(sales);

	    try{
	    	File file = new File(args[0],"branch.out");
		    FileWriter  fw = new FileWriter(file);
		    bw = new BufferedWriter(fw);
		    PrintWriter pw = new PrintWriter(bw);


		    for(Map.Entry<String, String> entry : sales.entrySet()){

		    	pw.println(entry.getKey() + "," + entry.getValue() + "," + map.get(entry.getKey()));

		   	}

		    //map.value 引き出し

		    }catch(IOException e){
		       	 System.out.println("予期せぬエラーが発生しました");
	        }finally{
				if(bw != null)
					try{
						bw.close();
						}catch(IOException e){
							System.out.println("closeできませんでした");
							}
				}
	}
}
